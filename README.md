# Introduction
Macbeth (MAChine / BEamline Timing Hardware) is a synchronization system aimed to synchronize the beamlines experimental devices (pump-probe, laser, …) with the accelerator beam with a high accuracy.

# Description
Macbeth hardware relies on AMD / Xilinx [Kria SOM K26](https://www.amd.com/en/products/system-on-modules/kria/k26.html) for FPGA/CPU part and on [TimIQ](https://ohwr.org/project/timiq/timiq-hw) design to achieve high accuracy phase shifting.

# Status
- Schematics: draft
- PCB: not yet
- Mechanical CAD: not yet
- FPGA firmware: not yet
- Embedded software: not yet
- Prototype test and validation: not yet
- Deployment: not yet

# Tools
![KiCad](images/kicad_logo_small.png) [KiCad](https://www.kicad.org)

# Licenses
[Licenses](licenses/license.txt)

---

![Synchrotron SOLEIL](images/soleil_logo.png)
