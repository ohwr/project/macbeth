License:

- The Macbeth system is released under CERN-OHL-S strongly-reciprocal license (see https://ohwr.org/project/cernohl/wikis/Documents/CERN-OHL-version-2)
- The KiCad libraries for the board are available on Github (https://github.com/Synchrotron-SOLEIL/KiCad-Lib). They are released under CC BY-SA 4.0 licence (see https://www.kicad.org/libraries/license/ ; https://creativecommons.org/licenses/by-sa/4.0/legalcode)
- The VHDL codes are released under GNU GPL licence version 3 (see https://www.gnu.org/licenses/gpl-3.0.txt)
- The Embedded codes are released under GNU GPL licence version 3 (see https://www.gnu.org/licenses/gpl-3.0.txt)
- Synchrotron SOLEIL's name and logo cannot be used without permission
